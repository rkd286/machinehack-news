from data_preprocessing import DataPreprocessor
from model import BertModelForSequenceClassification

def main():
    data = DataPreprocessor('input/Data_Train.xlsx', 'STORY', 'SECTION')
    train_dataloader, val_dataloader, test_dataloader = data.data_loader()

    model = BertModelForSequenceClassification(data.num_labels)

    model.train(
        train_dataloader, 
        val_dataloader
    )

    model.test(
        test_dataloader
    )

if __name__ == '__main__':
    main()