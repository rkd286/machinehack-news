import warnings
warnings.filterwarnings('ignore')

import pandas as pd

import torch
from torch.utils.data import TensorDataset, random_split, DataLoader, RandomSampler, SequentialSampler
from transformers import BertTokenizer

from config import BATCH_SIZE, SEQUENCE_LENGTH, VALIDATION_SIZE, TRAIN_SPLIT, TRAIN_TEST_SPLIT_SIZE

class DataPreprocessor:

    def __init__(self, path, sentence_column, label_column):
        self.path = path
        self.sentence_column = sentence_column
        self.label_column = label_column
        self.tokenizer = BertTokenizer.from_pretrained(
            'bert-base-uncased',
            do_lower_case=True
        )
        df = pd.read_excel(self.path)

        self.sentences = df[self.sentence_column].values
        self.labels = df[self.label_column].values
        
        self.max_len = 0
        self.num_labels = df[self.label_column].nunique()

        for sentence in self.sentences:
            input_ids = self.tokenizer.encode(
                sentence,
                add_special_tokens=True
            )
            self.max_len = max(self.max_len, len(input_ids))

        print('number of observations: {}'.format(len(self.sentences)))
        print('max length: {}'.format(self.max_len))
        print('number of classes: {}'.format(self.num_labels))
        

    
    def data_loader(self):        
        # We need to convert the tokens into Bert token IDs and we also need to create a list of attention masks
        
        max_length = SEQUENCE_LENGTH
        val_split_size = VALIDATION_SIZE
        train_split = TRAIN_SPLIT
        train_split_size = TRAIN_TEST_SPLIT_SIZE

        input_ids = []
        attention_masks = []

        if max_length is not None:
            self.max_len = max_length

        for sentence in self.sentences:
            encoded_dict = self.tokenizer.encode_plus(
                sentence,
                add_special_tokens=True,
                max_length=min(512, self.max_len),
                pad_to_max_length=True,
                return_attention_mask=True,
                return_tensors='pt' # For Pytorch
            )

            input_ids.append(encoded_dict['input_ids'])
            attention_masks.append(encoded_dict['attention_mask'])

        self.input_ids = torch.cat(input_ids, dim=0)
        self.attention_masks = torch.cat(attention_masks, dim=0)
        self.labels = torch.tensor(self.labels)

        self.dataset = TensorDataset(self.input_ids, self.attention_masks, self.labels)

        if train_split:
            train_size = int((1 - train_split_size)*len(self.dataset)) 
            test_size = len(self.dataset) - train_size
            val_size = train_size - int((1 - val_split_size)*train_size)
            train_size = int((1 - val_split_size)*train_size)
            
            train, val, test = random_split(self.dataset, [train_size, val_size, test_size])

            train_dataloader = DataLoader(
                train,
                sampler=RandomSampler(train),
                batch_size=BATCH_SIZE
            )

            val_dataloader = DataLoader(
                val,
                sampler=RandomSampler(val),
                batch_size=BATCH_SIZE
            )

            test_dataloader = DataLoader(
                test,
                sampler=SequentialSampler(test),
                batch_size=BATCH_SIZE
            )

            return (train_dataloader, val_dataloader, test_dataloader)
        
        else:
            train_size = int((1 - val_split_size)*len(self.dataset))
            val_size = len(self.dataset) - train_size

            train, val = random_split(self.dataset, [train_size, val_size])

            train_dataloader = DataLoader(
                train,
                sampler=RandomSampler(train),
                batch_size=BATCH_SIZE
            )

            val_dataloader = DataLoader(
                val,
                sampler=SequentialSampler(val),
                batch_size=BATCH_SIZE
            )

            return (train_dataloader, val_dataloader, None)
    
    def data_loader_test(self):
        max_length = SEQUENCE_LENGTH
        
        input_ids = []
        attention_masks = []

        if max_length is not None:
            self.max_len = max_length

        for sentence in self.sentences:
            encoded_dict = self.tokenizer.encode_plus(
                sentence,
                add_special_tokens=True,
                max_length=min(512, self.max_len),
                pad_to_max_length=True,
                return_attention_mask=True,
                return_tensors='pt' # For Pytorch
            )

            input_ids.append(encoded_dict['input_ids'])
            attention_masks.append(encoded_dict['attention_mask'])

        self.input_ids = torch.cat(input_ids, dim=0)
        self.attention_masks = torch.cat(attention_masks, dim=0)
        self.labels = torch.tensor(self.labels)

        self.dataset = TensorDataset(self.input_ids, self.attention_masks, self.labels)

        test_dataloader = DataLoader(
                            test,
                            sampler=SequentialSampler(val),
                            batch_size=BATCH_SIZE
                          )
        return test_dataloader


if __name__=='__main__':
    data = DataPreprocessor('Data_Train.xlsx', 'STORY', 'SECTION')
    dataloaders = data.data_loader()



    



