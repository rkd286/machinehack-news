import torch
import torch.nn as nn

import numpy as np
import os

from transformers import BertForSequenceClassification, AdamW, get_linear_schedule_with_warmup
from config import LEARNING_RATE, EPSILON, EPOCHS, BATCH_SIZE, OUTPUT_DIR
from sklearn.metrics import accuracy_score

class BertModelForSequenceClassification:

    def __init__(self, num_labels=2, test=False):
        
        torch.cuda.empty_cache()

        if not test:

            self.model = BertForSequenceClassification.from_pretrained(
                'bert-base-uncased',
                num_labels=num_labels,
                output_attentions=False,
                output_hidden_states=False
            )

            self.model.cuda()
        
        else:
            
            self.model = BertForSequenceClassification.from_pretrained(
                OUTPUT_DIR
            )

            self.model.cuda()

    
    def accuracy(self, preds, labels):
        
        preds = np.argmax(preds, axis=1).flatten()
        labels = labels.flatten()
        return accuracy_score(preds, labels)
    
    def train(self, train_dataloader, val_dataloader):
        
        device = 'cuda' if torch.cuda.is_available() else 'cpu'

        self.optimizer = AdamW(
            self.model.parameters(),
            lr=LEARNING_RATE,
            eps=EPSILON
        )

        # Weight decay
        total_steps = len(train_dataloader) * BATCH_SIZE

        
        self.scheduler = get_linear_schedule_with_warmup(
            self.optimizer,
            num_warmup_steps=0,
            num_training_steps=total_steps
        )

        for i in range(EPOCHS):
            print('################### EPOCH: {}/{} ###################'.format(i+1, EPOCHS))

            train_loss = 0
            eval_loss = 0
            eval_accuracy = 0

            self.model.train()

            for step, batch in enumerate(train_dataloader):

                if step % 40 == 0:
                    print('epoch: {}\tstep :{}\t'.format(i+1, step))
                
                batch_input_ids = batch[0].to(device)
                batch_attention_masks = batch[1].to(device)
                batch_labels = batch[2].to(device)

                self.model.zero_grad()

                loss, _ = self.model(
                    batch_input_ids,
                    token_type_ids=None,
                    attention_mask=batch_attention_masks,
                    labels=batch_labels
                )

                train_loss += loss.item()

                loss.backward()

                nn.utils.clip_grad_norm(self.model.parameters(), 1.0)

                self.optimizer.step()
                self.scheduler.step()
            
            avg_train_loss = train_loss / len(train_dataloader)
        
            print('#### VALIDATION ####')
            self.model.eval()

            for batch in val_dataloader:
                
                batch_input_ids = batch[0].to(device)
                batch_attention_masks = batch[1].to(device)
                batch_labels = batch[2].to(device)

                with torch.no_grad():

                    loss, logits = self.model(
                        batch_input_ids,
                        token_type_ids=None,
                        attention_mask=batch_attention_masks,
                        labels=batch_labels
                    )

                    eval_loss += loss.item()

                    logits = logits.detach().cpu().numpy()
                    batch_labels = batch_labels.detach().cpu().numpy()

                    eval_accuracy += self.accuracy(logits, batch_labels)
            
            avg_val_accuracy = eval_accuracy / len(val_dataloader)

            print('Train Loss: {}'.format(avg_train_loss))
            print('validation accuracy: {}'.format(avg_val_accuracy))
        
        output_dir = './saved/'

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        
        model_to_save = self.model.module if hasattr(self.model, 'module') else self.model  # Take care of distributed/parallel training
        model_to_save.save_pretrained(output_dir)
        # tokenizer.save_pretrained(output_dir)

        print('##################################### TRAINING COMPLETE #####################################')

    def test(self, test_dataloader):
        
        device = 'cuda' if torch.cuda.is_available() else 'cpu'
        
        test_accuracy = 0
        
        for batch in test_dataloader:

            batch_input_ids = batch[0].to(device)
            batch_attention_masks = batch[1].to(device)
            batch_labels = batch[2].to(device)

            with torch.no_grad():

                loss, logits = self.model(
                    batch_input_ids,
                    token_type_ids=None,
                    attention_mask=batch_attention_masks,
                    labels=batch_labels
                )

#                 eval_loss += loss.item()

                logits = logits.detach().cpu().numpy()
                batch_labels = batch_labels.detach().cpu().numpy()

                test_accuracy += self.accuracy(logits, batch_labels)
            
        avg_test_accuracy = test_accuracy / len(test_dataloader)
    
        print('test accuracy: {}'.format(avg_test_accuracy))










